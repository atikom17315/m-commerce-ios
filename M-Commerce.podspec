Pod::Spec.new do |s|
  s.name             = 'M-Commerce'
  s.version          = '0.1.0'
  s.summary          = 'MCommerce library for 7App'
  s.homepage         = 'https://bitbucket.org/atikom17315/m-commerce-ios'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Atikom Tancharoen' => 'atikom.apple@gmail.com' }
  s.source           = { :git => 'https://bitbucket.org/atikom17315/m-commerce-ios.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target   = '9.0'
  s.swift_version = '4.2'
  s.cocoapods_version = '>= 1.6.1'
  s.requires_arc = true

  s.source_files = 'Sources/*.swift'
  
  # s.resource_bundles = {
  #   'M-Commerce' => ['M-Commerce/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
