# M-Commerce

[![CI Status](https://img.shields.io/travis/TangMo2019/M-Commerce.svg?style=flat)](https://travis-ci.org/TangMo2019/M-Commerce)
[![Version](https://img.shields.io/cocoapods/v/M-Commerce.svg?style=flat)](https://cocoapods.org/pods/M-Commerce)
[![License](https://img.shields.io/cocoapods/l/M-Commerce.svg?style=flat)](https://cocoapods.org/pods/M-Commerce)
[![Platform](https://img.shields.io/cocoapods/p/M-Commerce.svg?style=flat)](https://cocoapods.org/pods/M-Commerce)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

M-Commerce is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'M-Commerce'
```

## Author

TangMo2019, tangmo@appsynth.net

## License

M-Commerce is available under the MIT license. See the LICENSE file for more info.
